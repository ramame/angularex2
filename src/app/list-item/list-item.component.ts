import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'ListItem',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() data:any;

  id;
  title;
  price;
  stock;
  setred=false;
  constructor() { }
 
  ngOnInit() {
    if (this.data.stock<10) {
      this.setred = true;
   }
    this.id= this.data.id;
    this.title=this.data.title;
    this.price=this.data.price;
    this.stock=this.data.stock;
    
  }

}
